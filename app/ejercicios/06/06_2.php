 <html>
  <head>
    <style>
      table, tr, td {
        border:  1px solid;
        border-collapse: collapse;
        text-align: center;
      }
    </style>
  </head>
  <body>
    <p>2. Implementar una clase que muestre una lista de hipervínculos en forma horizontal (básicamente un menú de opciones)</p>
    <p>//Metodo que añade un nuevo enlace al menu<br>cargarOpcion()</p>
    <p>//Metodo que visualiza el menu en una pagina HTML<br>mostrar()</p><br>
    <?php
		  class Menu {

        private $links=array();

        public function cargarOpcion($nombre,$link) {
          $this->links[]='<td><a href="' . $link . '">' . $nombre . '</a></td>';
        }

        public function mostrar() {

          ?><table><tr><?php

          $linksLength=count($this->links);
          for($i=0;$i<$linksLength;$i++) {
            echo $this->links[$i];
          }

          ?></tr></table><?php

        }

      }

      $menu1=new Menu();
      $menu1->cargarOpcion('Google','https://www.google.es/');
      $menu1->cargarOpcion('Wikipedia','https://es.wikipedia.org/wiki/Wikipedia:Portada');
      $menu1->cargarOpcion('W3Schools','https://www.w3schools.com/');
      $menu1->mostrar();
    ?>
  </body>
</html>