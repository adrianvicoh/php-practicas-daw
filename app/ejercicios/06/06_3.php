<html>
  <head>

  </head>
  <body>
    <p>3. Confeccionar una clase CabeceraPagina que permita mostrar un título, indicarle si queremos que aparezca centrado, a derecha o izquierda, además permitir definir el color de fondo y de la fuente. Todo esto son propiedades del objecto Titulo</p>
    <p>//Establece los valores de los atributos<br>inicializar()</p>
    <p>//Metodo que visualiza el menu en una pagina HTML<br>mostrar()</p><br>
    <?php
		class CabeceraPagina {

            private $titulo;
            private $colorFondo;
            private $colorFuente;
            private $alineado;

            public function inicializar($texto,$color,$fuente,$alineacion) {
              $this->titulo=$texto;
              $this->colorFondo=$color;
              $this->colorFuente=$fuente;
              $this->alineado=$alineacion;
            }

            public function mostrar() {
              echo '<h1 style="color:' . $this->colorFuente . ';background-color:' . $this->colorFondo . ';text-align:' . $this->alineado . ';">' . $this->titulo . '</h1>';
            }

        }
        $titulo1=new CabeceraPagina();
        $titulo1->inicializar('Hello World!','yellow','blue');
        $titulo1->mostrar();
    ?>
  </body>
</html>