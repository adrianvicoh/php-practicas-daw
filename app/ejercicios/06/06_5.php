<html>
  <head>

  </head>
  <body>
    <p>5. Confeccionar una clase Tabla que permita indicarle en el constructor la cantidad de filas y columnas. Definir otro metodo que podamos cargar un dato en una determinada fila y columna además de definir su color de fuente y fondo. Finalmente debe mostrar los datos en una tabla HTML</p>
    <?php
	class Tabla {
	  private $mat=array();
	  private $cantFilas;
	  private $cantColumnas;
	  public function __construct($fi,$co)
	  {
		//Hacer esta funcion
	  }

	  public function cargar($fila,$columna,$valor)
	  {
		$this->mat[$fila][$columna]=$valor;
	  }

	  private function inicioTabla()
	  {
		echo '<table border="1">';
	  }

	  private function inicioFila()
	  {
		echo '<tr>';
	  }

	  private function mostrar($fi,$co)
	  {
		echo '<td>'.$this->mat[$fi][$co].'</td>';
	  }

	  private function finFila()
	  {
		echo '</tr>';
	  }

	  private function finTabla()
	  {
		echo '</table>';
	  }

	  public function graficar()
	  {
		//Hacer esta funcion
	  }
	}

	$tabla1=new Tabla(2,3);
	$tabla1->cargar(1,1,"1");
	$tabla1->cargar(1,2,"2");
	$tabla1->cargar(1,3,"3");
	$tabla1->cargar(2,1,"4");
	$tabla1->cargar(2,2,"5");
	$tabla1->cargar(2,3,"6");
	$tabla1->graficar();
	?>
  </body>
</html>