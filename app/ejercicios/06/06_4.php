<html>
  <head>

  </head>
  <body>
    <p>4. Confeccionar una clase CabeceraPagina que permita mostrar un título, indicarle si queremos que aparezca centrado, a derecha o izquierda, además permitir definir el color de fondo y de la fuente. Pasar los valores que cargaran los atributos mediante un constructor.</p>
    <p>//No utilizar metodo de inicializar</p>
    <p>//Metodo que visualiza el menu en una pagina HTML<br>mostrar()</p><br>
    <?php
        
        class CabeceraPagina {

          private $titulo;
          private $colorFondo;
          private $colorFuente;
          private $alineacion;

          public function __construct($texto,$color,$fuente,$align) {
			$this->titulo=$texto;
            $this->colorFondo=$color;
            $this->colorFuente=$fuente;
			$this->alineacion=$align;
		  }
		  
		  public function mostrar() {
			echo '<h1 style="text-align:' . $this->alineacion . ';color:' . $this->colorFuente . ';background-color:' . $this->colorFondo . ';">' . $this->titulo . "</h1>";
		  }

        }
		
		$titulo1=new CabeceraPagina("Hello World!","yellow","blue","center");
		$titulo1->mostrar();
    ?>
  </body>
</html>