<html>
	<head>
		<style>
			table, td, tr {
    			border: 1px solid;
    			border-collapse: collapse;
			}
		</style>
	</head>
	<body>
		<table>
		<tr>
	        <th>Operación</td>
	        <td>Resultado</td>
      </tr>
      <tr>
        <td>
          <?php 
            $v1 = 2;
            $v2 = 4;
            echo $v1 . ' + ' . $v2 . ' = ' . ($v1 + $v2);
            echo "<br>";
            echo "$v1 + $v2 = " . ($v1 + $v2);
          ?>
        </td>
        <td>
          <?php echo $v1 + $v2; ?>
        </td>
      </tr>
      <tr>
        <td>
          <?php 
            echo "$v2 - $v1";
          ?>
        </td>
        <td>
          <?php echo $v2 - $v1; ?>
        </td>
      </tr>
      <tr>
        <td>
          <?php 
            echo "$v1 * $v2";
          ?>
        </td>
        <td>
          <?php echo $v1 * $v2; ?>
        </td>
      </tr>
      <tr>
        <td>
          <?php 
            echo "$v2 / $v1";
          ?>
        </td>
        <td>
          <?php echo $v2 / $v1; ?>
        </td>
      </tr>
    </table>

	</body>
</html>