<html>
  <head>
    
  </head>
  <body>
    <p>Una función que reciba cinco números enteros como parámetros y muestre por pantalla el resultado de sumar los cinco números (tipo procedimiento, no hay valor devuelto).</p><br>
    <?php
      function suma($num1, $num2, $num3, $num4, $num5) {
        echo "La suma de $num1, $num2, $num3, $num4, $num5 és " . ($num1 + $num2 + $num3 + $num4 + $num5);
      }
      suma(2,4,6,7,9);
    ?>
  </body>
</html>