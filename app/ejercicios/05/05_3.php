<html>
  <head>
    
  </head>
  <body>
    <p>3. Una función que reciba como parámetros el valor del radio de la base y la altura de un cilindro y devuelva el volumen del cilindro, teniendo en cuenta que el volumen de un cilindro se calcula como Volumen = númeroPi * radio * radio * Altura siendo númeroPi = 3.1416 aproximadamente.</p><br>
    <?php
      function volume($radiBase, $altura) {
        return pi() * $radiBase * $radiBase * $altura;
      }
      echo "Un cilindro de radio 4 y altura 5 tiene un volumen de " . volume(4,5);
    ?>
  </body>
</html>