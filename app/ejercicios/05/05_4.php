<html>
  <head>
    
  </head>
  <body>
    <p>4. Una función a la que se le pasa el precio de una compra y calcula el precio de venta mediante la siguientes reglas:</p>
    <p>Si la compra no alcanza los $100, no se realiza ningún descuento.</p>
    <p>Si la compra está entre $100 y $499,99, se descuenta un 10%.</p>
    <p>Si la compra supera los $500, se descuenta un 15%.</p>
    <?php
		$precioInicial = 150;
      function precioFinal($precio) {
        $final;
		    $descuento;
        if ($precio < 100) {
          $descuento = 0;
        } else if ($precio >= 100 && $precio < 500) {
          $descuento = 10;
        } else ($precio >= 500) {
			    $descuento = 15;
        }
      $final = $precio - $precio * $descuento / 100;
      return $final;
      }
	  $precioVenta = precioFinal($precioInicial);
      echo "<br><p>Con un precio inicial de $precioInicial €, el precio de venta es de $precioVenta €</p>";
    ?>
  </body>
</html>