<html>
  <head>
    
  </head>
  <body>
    <p>7. Realiza una función separar(array) que tome un array de números enteros y devuelva (no imprimir) dos listas ordenadas. La primera con los números pares y la segunda con los números impares.</p>
    <?php
	
		function separar($numeros) {
			sort($numeros);
			$pares = array();
			$impares = array();
			$numerosLength = count($numeros);
			for ($i = 0; $i < $numerosLength - 1; $i++) {
				if ($numeros[$i] % 2 == 0) {
				$pares[] = $numeros[$i];
			} else {
				$impares[] = $numeros[$i];
			}
		}
			$separado = array($pares,$impares);
			for ($i = 0;$i < count($separado); $i++) {
				for ($j = 0;$j < count($separado[$i]); $j++) {
					echo $separado[$i][$j] . ", ";
				}
				echo "</br>";
			}
			return $separado;
		}
		$numeros = array(-12, 84, 13, 20, -33, 101, 9);
		$separado = separar($numeros);
		
    ?>
  </body>
</html>