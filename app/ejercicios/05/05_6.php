<html>
  <head>
    
  </head>
  <body>
    <p>6. Realiza una función llamada intermedio(a, b) que a partir de dos números, devuelva su punto intermedio. Cuando lo tengas comprueba el punto intermedio entre -12 y 24</p>
    <?php
		function intermedio($punto1, $punto2) {
			return ($punto1 + $punto2) / 2;
		}
		$num1 = -12;
		$num2 = 24;
		$intermidiate = intermedio($num1, $num2);
		echo "El punto medio entre $num1 y $num2 es $intermidiate";
    ?>
  </body>
</html>