<!doctype html>
<html>
<?php
include_once('_header.php');
include('productsLoader.php');
?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Product List</title>
    <!-- Styles & JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
</head>

<body>


<div class="row">

  <div class="col-lg-3">

    <h1 class="my-4">Resumen</h1>
    <ul class="list-group">
      <li class="list-group-item">
        Total de productos: <?php echo $_SESSION["quantity"];?></li>
      <li class="list-group-item">
        Precio total de productos: <?php echo $_SESSION["price"];?></li>
    </ul>



  </div>
  <!-- /.col-lg-3 -->

  <div class="col-lg-9">
    <div class="row">

    <!-- Por cada producto en el carrito -->
    <?php
    $newStock = new ProductsLoader();
    $products = $newStock->getAllProducts();
    foreach ($_SESSION["cart"] as $selItemInfo) {?>
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="card h-100">
          <a href="#"><img class="card-img-top" src="<?php echo $products[$selItemInfo["id"]]->getImage();?>" alt=""></a>
          <div class="card-body">
            <h3 class="card-title">
              <a href="#"><?php echo $products[$selItemInfo["id"]]->getName();?></a>
            </h3>
            <h4>
              <?php
              if ($selItemInfo["action"] == "buy") {
                $internalPrice=$products[$selItemInfo["id"]]->getSellPrice();
                echo "Compra";
                
              } else {
                $internalPrice=$products[$selItemInfo["id"]]->getRentPrice();
                echo "Alquiler";
              }?>
            </h4>
            <h5>
              <?php
              echo $internalPrice;?>
            </h5>
          </div>
        </div>
      </div>
      <?php } ?>

      <!-- SI no hay productos -->
      <?php if (empty($_SESSION["cart"])) {?>
        <p>No products to show</p>
      <?php } ?>
      

    </div>
    <!-- /.row -->

  </div>
  <!-- /.col-lg-9 -->
</div>
