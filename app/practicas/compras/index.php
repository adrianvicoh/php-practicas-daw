<!doctype html>
<html>
<?php
include_once('_header.php');
include('productsLoader.php');
?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Product List</title>
    <!-- Styles & JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
</head>

<body>



    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="{{url('/')}}">Product's List</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
    </nav>

    <!-- Page Content -->
    <div class="container m-3">
    <div class="row">
      
     <!--
     *  Por cada producto
      *-->
    <?php
    $newStock = new ProductsLoader();
    $assignedNum = 0;
    foreach ($newStock->getAllProducts() as $nextProduct) {?>
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="card h-100">
          <a href="#"><img class="card-img-top" src="<?php echo $nextProduct->getImage();?>" alt=""></a>
          <div class="card-body">
            <h4 class="card-title">
              <a href="#"><?php echo $nextProduct->getName();?></a>
            </h4>
            <h5><?php echo $nextProduct->getSellPrice();?></h5>
            <p class="card-text">Descripcion</p>
          </div>
          <div class="card-footer">
            <form class="addCart" action="cartManager.php"  method="post" enctype="multipart/form-data">
              <input type="hidden" name="id" value="<?php echo $assignedNum;?>">
              <input type="hidden" name="quantity" value="1">
              <input type="hidden" name="action" value="buy">
              <input type="hidden" name="price" value="<?php echo $nextProduct->getSellPrice();?>">
              <button type="submit" class="btn-block btn-primary">Comprar</button>
            </form>
            <br>
            <form class="addCart" action="cartManager.php"  method="post" enctype="multipart/form-data">
              <input type="hidden" name="id" value="<?php echo $assignedNum;?>">
              <input type="hidden" name="quantity" value="1">
              <input type="hidden" name="action" value="rent">
              <input type="hidden" name="price" value="<?php echo $nextProduct->getRentPrice();?>">
              <button type="submit" class="btn-block btn-primary">Alquilar</button>
            </form>
          </div>
        </div>
      </div>
      <?php $assignedNum++;}?>
       <!-- SI NO HAY PRODUCTOS -->
      <p>No products to show</p>

    </div>
    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; Escola del Treball 2022</p>
        </div>
        <!-- /.container -->
    </footer>

</body>

</html>