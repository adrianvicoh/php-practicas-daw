<?php
session_start();
if (!isset($_SESSION["quantity"])) {
    $_SESSION["cart"]= array();
    $_SESSION["price"]=0;
    $_SESSION["quantity"]=0;
}
?>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="index.php">Tienda</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Numero de productos: <span id="carrito"><?php echo $_SESSION["quantity"];?></span> | </a>
                       
                    </li>
                    <li class="nav-item active"> <a class="nav-link" href="clearCart.php">Vaciar carrito  |
                        </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="resumen.php">Finalizar compra
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>