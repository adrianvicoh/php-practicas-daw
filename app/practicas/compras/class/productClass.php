<?php
include_once('_header.php');
class Product {

    private $name;
    private $sellPrice;
    private $rentPrice;
    private $image;

    function __construct($name, $sellPrice, $rentPrice)
    {
        $this->name = $name;
        $this->sellPrice = $sellPrice;
        $this->rentPrice = $rentPrice;
        $this->image = $_SERVER["root"] . "/practicas/compras/img/" . $name . ".jpg";
    }

    public function getName(){
        return $this->name;
    }

    public function getSellPrice(){
        return $this->sellPrice;
    }

    public function getRentPrice(){
        return $this->rentPrice;
    }

    public function setImage($image) {
        $this->image = $image;
    }

    public function getImage() {
        return $this->image;
    }
}