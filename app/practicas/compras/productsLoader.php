<?php
include_once('_header.php');
include('class/productClass.php');
class ProductsLoader {
    private $products = [];
    private $fileName = 'assets/products.db';

    function __construct() {
        $this->loadProducts();
    }

    public function loadProducts() {
        $newFile = fopen($this->fileName,'r');
        //$counter = 0;
        while(!feof($newFile)) {
            $newLine = trim(fgets($newFile));
            $productInfo = explode('###',$newLine);
            $nextProduct = new Product($productInfo[0],$productInfo[1],$productInfo[2]);
            //$nextProduct->setImage('img/osito.webp');
            //$this->products[] = ($nextProduct);
            array_push($this->products,$nextProduct);
        }
        fclose($newFile);
    }

    public function getAllProducts() {
        return $this->products;
    }

    public function getProduct($key) {
        return $this->products[$key];
    }
}