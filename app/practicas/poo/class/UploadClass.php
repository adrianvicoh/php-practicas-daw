<html>
	<head>
    
	</head>
  <body>
    <?php
        class Upload
        {

          private $fileName;
          private $filePath;

            /*
          * Constructor: Inicia la subida del archivo
          * Entrada:
          *   $name: Nombre del elemento del formulario del que gestionamos el $_FILE
          */
            function __construct($name){
                $this->upload($name);
            }
        
          /*
          * upload: Función que hace las operaciones necesarias para subir el archivo
          * al servidor
          */
          public function upload($name){
            $root = $_SERVER['DOCUMENT_ROOT'] . '/uploads';
            $tempPath = $_FILES[$name]['tmp_name'];
            $this->fileName = $_FILES[$name]['name'];
            move_uploaded_file($tempPath, $root . '/' . $this->fileName);
            $this->filePath = $root . '/' . $this->fileName;
          }
        
            /*
          * Getters. Lo que quiere decir que los atributos de la clase son private
          */
            public function getPath(){
              return $this->filePath;
            }

            public function getName() {
              return $this->fileName;
            }
        
        
        }
          /*
          * OPCIONAL:
          * Clase personalizada extendida de Exception que utilizaremos para lanzar errores
          * en la subida de archivos. Por ejemplo:
          * throw new UploadError("Error: Please select a valid file format.");
          */
          class UploadError extends Exception{}
    ?>
  </body>
</html>