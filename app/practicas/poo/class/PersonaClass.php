<html>
	<head>
    
	</head>
  <body>
    <?php
        class Persona
        {
            private $name;
            private $surname;
            private $picture;
            private $address;
        
            /*
            * Setters. Para añadir y modificar valores
            */
            public function setName($newName){
                $this->name = $newName;
            }
            public function setSurname($newSurname){
                $this->surname = $newSurname;
            }
            public function setPicture($newPicture){
                $this->picture = $newPicture;
            }
            public function setAddress($newAddress){
                $this->address = $newAddress;
            }
        
            /*
          * Getters. Lo que quiere decir que los atributos de la clase son private
          */
            public function getName(){
                return $this->name;
            }
            public function getSurname(){
                return $this->surname;
            }
            public function getPicture(){
                return $this->picture;
            }
            public function getAddress(){
                return $this->address;
            }
        }
    ?>
  </body>
</html>