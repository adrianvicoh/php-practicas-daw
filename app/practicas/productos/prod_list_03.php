<html>
  <head>
    <style>
        tr, th, td, table {
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
  </head>
  <body>
    <p>2. Crear una aplicació composta per 3 paginas PHP amb formularis que permeti ingresar el nom y preu d'una llista de productes.</p>
    <p>Tercera página(prod_list_03.php): Taula on llista tots els productes. Si algun producte no s'ha introduit ni nom ni preu ha de apareixer la frase "Product not inserted"</p><br>
    <table>
        <th>
            <td>#</td>
            <td>Product Name</td>
            <td>Price</td>
        </th>
        <?php
            $prodName = $_POST["productsName"];
            $prodPrice = $_POST["productsPrice"];
            for($i=0;$i<count($prodName);$i++) {
                echo '<tr>';
                echo '<td>' . $i . '</td>';
                echo '<td>' . $prodName[$i] . '</td>';
                echo '<td>' . $prodPrice[$i] . '</td>';
                echo '</tr>';
            }
        ?>
    </table>
  </body>
</html>