<?php
include_once('_header.php');
?>
<div class="row">
    <!--
       Recorre la variable galeria para construir la estructura HTML con las fotos y los titulos
    -->


    <?php
    if (sizeof($gallery->getGallery()) == 0) { ?>
        <div class="alert alert-primary" role="alert">
            The gallery is empty
        </div>
    <?php
    }
    ?>
</div>
<?php include_once('_footer.php') ?>