<?php
define("MAX_SIZE", 5); //Max Mb
define("UPLOAD_FOLDER", "pictures/");
define("PICTURES_LIST", "pictures/list.txt");

class UploadError extends Exception
{
}
   /*
    private $fileName // Nombre del campo definido en el formulario
    private $file = // Ruta donde encontrar el fichero subido (la ruta se guarda en fotos.txt)
    private $error = // Se guarda el mensaje concreto  para mostrar al usuario
   */
class Upload
{
    private $fileName = null;
    private $file = null;
    private $error = null;

    /*
    * Constructor. NO tocar
     */
    function __construct($fileName)
    {
        $this->fileName = $fileName;
        $this->uploadPicture();
    }

    /*
    * Función que se encarga de subir un archivo y moverlo a la carpeta /pictures
    * que almacena todas las fotos.
    * Return: Devuelve la ruta final del archivo.
    */
    function uploadPicture()
    {
        try {
            //Check if the folder exist is a valid picture and title
            //$_SERVER['DOCUMENT_ROOT'] return the folder of the server where is our APP
            if($_SERVER["REQUEST_METHOD"] == "POST") {
                
                
                // Check if file was uploaded without errors
                if(isset($_FILES["picture"]) && $_FILES["picture"]["error"] == 0) {
                    throw new UploadError("Error de subida del documento");
                }

                // Verify file extension
                $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
                $ext = pathinfo($this->filename, PATHINFO_EXTENSION);
                if (!in_array($ext, $allowed)) {
                    throw new UploadError("La extensión de archivo no es válida");
                }

                // Verify file size - 5MB maximum
                if ($_FILES["picture"]["size"] > MAX_SIZE) {
                    throw new UploadError("El tamaño de archivo supera 5MB");
                }

                // Verify MYME type of the file
                if (!in_array($_FILES["picture"]["type"], $allowed)) {
                    throw new UploadError("Tipo de archivo no válido");
                }

                // Check whether file exists before uploading it
                if(!file_exist($_SERVER['DOCUMENT_ROOT'] . '/' . UPLOAD_FOLDER . $this->fileName)) {
                    throw new UploadError("El documento ya existe");
                }
            
                // IF NO errors, then move the picture to Folder
                move_uploaded_file($_FILES["picture"]["tmp_name"], UPLOAD_FOLDER . $filename);
                return UPLOAD_FOLDER . $filename;

            }
        } catch (UploadError $e) {
            $this->error = $e->getMessage();
        } catch (Exception $e) {
            $this->error = $e->getMessage();
        }
    }

    /*
    * Función que se encarga de añadir al archivo fotos.txt el titulo y la ruta de la
    * fotografía recien subida
    * Entradas:
    *       $file_uploaded: La ruta del archivo
    *       $title_uploaded: El titulo del archivo
    * Return: null
    */
    function addPictureToFile($file_uploaded, $title_uploaded)
    {
        try {
            
            
        } catch (Exception $e) {
            $this->error = $e->getMessage();
        }
    }

    /*
    * Getters: No tocar
     */
    function getError()
    {
        return $this->error;
    }
    function getFile()
    {
        return $this->file;
    }
}
